#version 330 core
precision highp float;

layout(location = 0) out vec4 FragColor;

in vec2 CoordSrc;

uniform sampler2D img;
uniform sampler2D mask;

uniform float maskTreshhold = 0.7;

void main(){
    vec4 maskVal = texture(mask, CoordSrc);
    //vec4 colVal = texture(img, CoordSrc);
    //FragColor = vec4(mix(colVal.rgb, maskVal.rgb, maskVal.a), colVal.a+maskVal.a);
    
    if(maskVal.a < maskTreshhold){
        vec4 colVal = texture(img, CoordSrc);
        FragColor = colVal;
    }else{
        FragColor = vec4(0.0,0.0,0.0,0.0);
    }
    
}

